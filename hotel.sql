-- MariaDB dump 10.17  Distrib 10.4.10-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: hotel
-- ------------------------------------------------------
-- Server version	10.4.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archive`
--

DROP TABLE IF EXISTS `archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive` (
  `chambre` int(11) NOT NULL,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `codepostal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive`
--

LOCK TABLES `archive` WRITE;
/*!40000 ALTER TABLE `archive` DISABLE KEYS */;
INSERT INTO `archive` VALUES (13,'toto','itti','momo',58000),(13,'toto','itti','momo',58000),(2,'sdfsdf','','',0),(6,'toto','itti','momo',58000),(8,'toto','itti','momo',58000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(5,'dupond','jean','paris',75000),(14,'toto','itti','momo',58000),(17,'zorro','lucien','monterey',99),(16,'zorro','lucien','monterey',99),(15,'truc','machine','bidule',10),(15,'truc','machine','bidule',10),(14,'toto','itti','momo',58000),(13,'toto','itti','momo',58000),(5,'dupond','jean','paris',75000),(14,'toto','itti','momo',58000),(28,'Mauve','guy','48 rue des lilas',12),(28,'Mauve','guy','48 rue des lilas',12),(27,'Mauve','guy','48 rue des lilas',657000),(26,'Mauve','guy','48 rue des lilas',657000),(20,'zorro','lucien','monterey',99),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(2,'marc','zanzibar','test update = 1',75),(21,'aaa','aaaa','aaaaaaaaaa',65),(22,'aaa','aaaa','aaaaaaaaaa',65),(24,'aaa','aaaa','aaaaaaaaaa',65),(2,'toto','titi','roro',67000);
/*!40000 ALTER TABLE `archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `idprim` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `codepostal` int(11) NOT NULL,
  `dispo` int(11) NOT NULL,
  PRIMARY KEY (`idprim`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'kiki','test','57 ',67000,1),(2,'toto','titi','roro',67000,0);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-21 13:41:08
